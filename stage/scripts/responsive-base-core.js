/* jshint -W057 */
/* jshint -W058 */

//
//  "J"
//

var J = new function () {
    "use strict";

    this.name = "Jetshop Responsive Base Framework";
    this.version = "1.4.0";
    this.responsive = true; // use bool J.responsive to determine if framework is present
    this.cart = false;
    this.data = {
        currency: false,
        culture: false,
        priceList: false,
        priceListId: false,
        categoryId: false,
        productId: false,
        language: false,
        manufacturerList: false,
        jetshopData: false
    };
    this.config = {
        cartUrl: "/checkout",
        cartRESTUrl: "/Services/CartInfoService.asmx/LoadCartInfo",
        cssFallbackUrl: "/M1/Production/css/responsive-base.css", // Used for injection in pages lacking script management
        apiCachePrefix: "cache-",
        foundationConfig: {
            equalizer: {
                equalize_on_stack: true
            }
        },
        sameHeightConfig: {
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        }
    };
    this.checker = {
        isStage: false,
        isLoggedIn: false,
        isTouch: false,
        isStartPage: false,
        isProductPage: false,
        isCategoryPage: false,
        isCategoryAdvancedPage: false,
        isNewsPage: false,
        isSearchResultPage: false,
        isCheckoutPage: false,
        isStandardPage: false,
        isOrderConfirmationPage: false,
        isManufacturerPage: false,
        isManufacturerAdvancedPage: false,
        isSiteMapPage: false,
        isMyPage: false,
        isVatIncluded: false,
        isCheckoutHttps: false
    };
    this.translations = [
        {
            cart: {
                sv: "Kundvagn",
                en: "Cart",
                da: "Indkøbskurv",
                nb: "Handlevogn",
                fi: "Ostoskärry"
            }
        }
    ];
    this.translate = function (translationObject) {
        for (var key in J.translations) {
            if (translationObject in J.translations[key]) {
                if (typeof J.translations[key][translationObject][J.data.language] !== "undefined") {
                    return J.translations[key][translationObject][J.data.language];
                }
                else if (typeof J.translations[key][translationObject]["en"] !== "undefined") {
                    console.log("No translation found for '" + translationObject + "' in language " + J.data.language + ". Returning English.");
                    return J.translations[key][translationObject]["en"];
                }
                else if (typeof J.translations[key][translationObject]["sv"] !== "undefined") {
                    console.log("No translation found for '" + translationObject + "' in language " + J.data.language + ". Returning Swedish.");
                    return J.translations[key][translationObject]["sv"];
                } else {
                    console.log("No translation found for '" + translationObject + "' in language " + J.data.language);
                }
            }
        }
        console.log("No translation found for " + translationObject + "'");
        return false;
    };
    this.setPageType = function () {
        if (typeof JetshopData !== "undefined") {
            if (JetshopData.IsStartPage || $("html.page-default").length) {
                this.checker.isStartPage = true;
                return "start-page";
            } else if ($(".page-showproduct").length > 0) {
                this.checker.isProductPage = true;
                return "product-page";
            } else if ($(".page-listproductswithimageadvanced").length > 0) {
                this.checker.isCategoryAdvancedPage = true;
                return "category-advanced-page";
            } else if ($(".page-listproducts").length > 0) {
                this.checker.isCategoryPage = true;
                return "category-page";
            } else if ($("html.page-responsive-orderconfirmed").length > 0) {
                this.checker.isOrderConfirmationPage = true;
                return "orderconfirmation-page";
            } else if ($("html.page-orderdetailsconfirmed").length > 0) {
                this.checker.isOrderConfirmationPage = true;
                return "orderconfirmation-page";
            } else if ($("html.page-listmanufacturerproductswithimageadvanced").length > 0) {
                this.checker.isManufacturerAdvancedPage = true;
                return "manufacturer-advanced-page";
            } else if ($("html.page-listmanufacturerproducts").length > 0) {
                this.checker.isManufacturerPage = true;
                return "manufacturer-page";
            } else if ($("html.page-shownews").length > 0) {
                this.checker.isNewsPage = true;
                return "news-page";
            } else if ($("html.page-responsive-checkout").length > 0) {
                this.checker.isCheckoutPage = true;
                return "checkout-page";
            } else if ($("html.page-checkout").length > 0) {
                this.checker.isCheckoutPage = true;
                return "checkout-page";
            } else if ($("html.page-showsearchresult").length > 0) {
                this.checker.isSearchResultPage = true;
                return "searchresult-page";
            } else if ($("html.page-mypages").length > 0) {
                this.checker.isMyPage = true;
                return "my-page";
            } else if ($("html.page-showpage").length > 0) {
                this.checker.isStandardPage = true;
                return "standard-page";
            } else if ($("html.page-sitemap").length > 0) {
                this.checker.isSiteMapPage = true;
                return "sitemap-page";
            } else {
                // Default pageType
                this.checker.isStandardPage = true;
                return "standard-page";
            }
        } else {
            return false;
        }
    };
    this.switch = {
        width: "",
        queues: {
            small: [],
            mediumUp: [],
            medium: [],
            largeUp: []
        },
        addToSmall: function (queueItem) {
            this.queues.small.push(queueItem);
        },
        addToMediumUp: function (queueItem) {
            this.queues.mediumUp.push(queueItem);
        },
        addToMedium: function (queueItem) {
            this.queues.medium.push(queueItem);
        },
        addToLargeUp: function (queueItem) {
            this.queues.largeUp.push(queueItem);
        },
        check: function () {
            var queries = Foundation.media_queries;
            if (window.matchMedia(queries.xxlarge).matches) this.set("xxlarge");
            else if (window.matchMedia(queries.xlarge).matches) this.set("xlarge");
            else if (window.matchMedia(queries.large).matches) this.set("large");
            else if (window.matchMedia(queries.medium).matches) this.set("medium");
            else if (window.matchMedia(queries.small).matches) this.set("small");
        },
        set: function (width) {
            if (width != this.width) {
                var oldWidth = this.width;
                if (oldWidth === "small") executeQueue(this.queues.mediumUp);
                if (width === "small") executeQueue(this.queues.small);
                else if (width === "medium") executeQueue(this.queues.medium);
                else if (Foundation.utils.is_large_up()) {
                    if (oldWidth === "large" || oldWidth === "xlarge" || oldWidth === "xxlarge") {
                        var noop;
                    } else {
                        executeQueue(this.queues.largeUp);
                    }
                }
                $(window).trigger('switch', [width, oldWidth]);
                $("body").removeClass(removeClassRegexp(/^device-width-/)).addClass("device-width-" + width);
                this.width = width;
            }
        }
    };
    this.components = {
        browserDetect: function () {
            J.browser = bowser;
            addBodyClass("browser-" + convertStringToURLFriendly(bowser.name));
            if (bowser.msie) {
                addBodyClass("msie");
                if (bowser.version == 7) addBodyClass("ie7 msie-old");
                if (bowser.version == 8) addBodyClass("ie8 msie-old");
                if (bowser.version == 9) addBodyClass("ie9");
                if (bowser.version == 10) addBodyClass("ie10");
                if (bowser.version == 11) addBodyClass("ie11");
            } else {
                addBodyClass("not-msie");
            }
            if (bowser.webkit) addBodyClass("webkit");
            if (bowser.gecko) addBodyClass("gecko");
            if (bowser.ios) addBodyClass("ios");
            if (bowser.android) addBodyClass("android");
            if (bowser.mobile) addBodyClass("mobile");
            if (bowser.tablet) addBodyClass("tablet");
            if (bowser.tablet || bowser.mobile) J.checker.isTouch = true;
        },

        initFoundation: function () {
            $(document).foundation(J.config.foundationConfig);
        },

        registerHandlebarHelpers: function () {
            Handlebars.registerHelper('translate', function (obj) {
                var str = arguments[0];
                return J.translate(str);
            });
            Handlebars.registerHelper('currentCulture', function () {
                if (J.data.jetshopData) return J.data.culture;
            });
            Handlebars.registerHelper('currentLanguage', function () {
                if (J.data.jetshopData) return J.data.language;
            });
            Handlebars.registerHelper('isVatIncluded', function () {
                if (J.data.jetshopData) return J.checker.isVatIncluded
            });
            Handlebars.registerHelper('isStage', function () {
                if (J.data.jetshopData) return J.checker.isStage;
            });
            Handlebars.registerHelper('unescape', function (obj) {
                var doc = new DOMParser().parseFromString(arguments[0], "text/html");
                return doc.documentElement.textContent;
            });
            Handlebars.registerHelper("ifValue", function (conditional, options) {
                if (conditional == options.hash.equals) {
                    return options.fn(this);
                } else {
                    return options.inverse(this);
                }
            });
            Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
                switch (operator) {
                    case '==':
                        return (v1 == v2) ? options.fn(this) : options.inverse(this);
                    case '===':
                        return (v1 === v2) ? options.fn(this) : options.inverse(this);
                    case '!=':
                        return (v1 != v2) ? options.fn(this) : options.inverse(this);
                    case '!==':
                        return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                    case '<':
                        return (v1 < v2) ? options.fn(this) : options.inverse(this);
                    case '<=':
                        return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                    case '>':
                        return (v1 > v2) ? options.fn(this) : options.inverse(this);
                    case '>=':
                        return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                    case '&&':
                        return (v1 && v2) ? options.fn(this) : options.inverse(this);
                    case '||':
                        return (v1 || v2) ? options.fn(this) : options.inverse(this);
                    default:
                        return options.inverse(this);
                }
            });
        },

        getManufacturerList: function () {
            if ($(".manufacturer-selector-wrapper").length > 0) {
                J.data.manufacturerList = [];
                $('.manufacturer-selector-body option').each(function () {
                    var temp = {
                        id: $(this).val(),
                        name: $(this).context.innerHTML,
                        url: "/-m-" + $(this).val() + ".aspx"
                    };
                    J.data.manufacturerList.push(temp);
                });
                J.data.manufacturerList.splice(0, 1);
                //$(".manufacturer-selector-wrapper").remove();
            }
        },
        
        startPageObjects: function () {
            //TODO this code is crap, please rewrite me
            if ($("#startpage_list").length > 0) {
                var data = [];
                var groupedItemList = {};
                var objects = $('#startpage_list div[class*="item-"]');
                objects.each(function () {
                    var classes = $(this).attr("class").toString().split(' ');
                    var item = classes.filter(function (item) {
                        return item.indexOf("item-") > -1;
                    });
                    var rowNumber = item[0].split("-")[1];
                    $(this).wrap("<li class='li-group-" + rowNumber + "'></li>");
                    $(this).addClass("group-" + rowNumber + " startpage-object product-wrapper");
                    data.push(rowNumber);
                });
                for (var x = 0; x < data.length; x++) {
                    var row = data[x];
                    if (!groupedItemList[row]) {
                        groupedItemList[row] = [];
                    }
                    groupedItemList[row].push(data[x]);
                }
                for (var key in groupedItemList) {
                    var len = groupedItemList[key].length;
                    $(".li-group-" + key)
                        .wrapAll("<ul class='row-line row-line-" + key + " items-" + len + " resetfloat' />")
                        .matchHeight();
                }
                $('.startpage-product-item .product-name').matchHeight();
            }
        },

        tabSystem: function () {
            if ($("#ctl00_ctl00_main_rightmain_ctl00_ctl00_tabContent_pnlProductDescription_tab").length > 0) {
                var descriptionTab = $("#ctl00_ctl00_main_rightmain_ctl00_ctl00_tabContent_pnlProductDescription_tab");
                var descriptionContent = $("#ctl00_ctl00_main_rightmain_ctl00_ctl00_tabContent_pnlProductDescription");
                var specificationsTab = $("#__tab_ctl00_ctl00_main_rightmain_ctl00_ctl00_tabContent_pnlProductSpecifications");
                var relatedTab = $("#__tab_ctl00_ctl00_main_rightmain_ctl00_ctl00_tabContent_pnlRelatedProducts");
            } else {
                var descriptionTab = $("#ctl00_main_ctl00_ctl00_tabContent_pnlProductDescription_tab");
                var descriptionContent = $("#ctl00_main_ctl00_ctl00_tabContent_pnlProductDescription_productDescription");
                var specificationsTab = $("#ctl00_main_ctl00_ctl00_tabContent_pnlProductSpecifications_tab");
                var relatedTab = $("#ctl00_main_ctl00_ctl00_tabContent_pnlRelatedProducts_tab");
            }
            var specificationsContent = $(".template-wrapper");
            var relatedContent = $(".relatedTable");
            var hasDescriptionContent = false;
            var hasSpecificationsTab = false;
            var hasRelatedTab = false;
            $(".product-tabs").after("<div id='tab-wrapper' class='resetfloat'></div>");
            if (descriptionContent.text().length > 0) {
                hasDescriptionContent = true;
            }
            if (specificationsTab.length > 0) {
                hasSpecificationsTab = true;
            }
            if (relatedTab.length > 0) {
                hasRelatedTab = true;
            }
            function hideTabSystem() {
                $(".product-tabs").hide();
                $(".product-toolbar").addClass("no-product-tabs");
            }

            function renderTab(classes, content, header) {
                var str = "";
                str += "<div class='tab-item " + classes + "'>";
                str += "<h2 class='tab-item-header'>" + header + "</h2>";
                str += "<div class='tab-item-inner'>";
                str += "</div>";
                str += "</div>";
                $("#tab-wrapper").append(str);
                $("#tab-wrapper .tab-item:last-child .tab-item-inner").append(content);
            }

            if (hasDescriptionContent === false && hasSpecificationsTab === false && hasRelatedTab === false) {
                hideTabSystem();
            }
            if (hasDescriptionContent) {
                renderTab("tab-description", descriptionContent, descriptionTab.text());
            }
            if (hasSpecificationsTab) {
                renderTab("tab-specification template-wrapper", specificationsContent, specificationsTab.text());
            }
            if (hasRelatedTab) {
                renderTab("tab-related", relatedContent, relatedTab.text());
            }
            $(".product-tabs").hide();
        },

        productThumbnails: function () {
            $("a.ProductThumbnail").each(
                function () {
                    $(this).wrap("<li></li>");
                }
            );
            $(".ProductThumbnailsBody li").wrapAll("<ul class='small-block-grid-3 medium-block-grid-4 large-block-grid-5 xlarge-block-grid-6'></ul>");
        },

        leftCategoryMenu: function () {
            var categoryNav = $("#category-navigation");
            $("#left-nav-placeholder").prepend("<nav role='navigation' id='left-cat-nav'><ul class='lv1'>" + categoryNav.html() + "</ul></nav>");
        },

        pagingControll: function () {
            if ($(".paging-control-box .showall").length > 0) {
                $(".paging-control.lower").append("<div id='paging-control-switch'></div>");
                $("#paging-control-switch").append($(".paging-control-box .showall"));
                $(".paging-control-box .showall").remove();
            }
        },

        jetshopLogo: function () {
            $(".infoTextLogo img").attr("src", "/stage/images/responsive-base/powered-by-jetshop-lightgrey.png");
            // Available colors are:
            // powered-by-jetshop-darkgrey.png, powered-by-jetshop-lightgrey.png, powered-by-jetshop-white.png, powered-by-jetshop-black.png
        },

        footer: function () {
            var footerBoxCount = $(".footer-box").length;
            $("#footer").addClass("footer-box-count-" + footerBoxCount);
        },

        removeEmptyCurrencySelector: function () {
            var currencySelector = $("#footer").find(".currency-selector-wrapper");
            if (currencySelector.length) {
                if ($(".currency-selector-item").length) {
                    currencySelector.addClass("currency-selector-radios");
                } else {
                    currencySelector.addClass("currency-selector-dropdown");
                    var currencySelectorOptions = currencySelector.find("option");
                    if (currencySelectorOptions.length < 2) {
                        currencySelector.remove()
                    }
                }
            }
        },

        removeEmptyCultureSelector: function () {
            var cultureSelector = $("#footer").find(".culture-selector-wrapper");
            if (cultureSelector.length) {
                if (cultureSelector.find(".culture-selector-clickable").length) {
                    // Type: Flags
                    if (cultureSelector.find("input").length < 2) {
                        cultureSelector.remove();
                    }
                } else if (cultureSelector.find("option").length) {
                    // Type: DropDown
                    cultureSelector.addClass("culture-selector-dropdown");
                    if (cultureSelector.find("option").length < 2) {
                        cultureSelector.remove();
                    }
                } else {
                    // Type: Text
                    if (cultureSelector.find("a").length < 2) {
                        cultureSelector.remove();
                    }
                }
            }
        },

        removeEmptyAttributeWrappers: function () {
            $(".product-attributes > div").each(function () {
                if (!$(this).find("select").length) {
                    $(this).remove();
                }
            });
        },

        removeEmptySpecificationWrapper: function () {
            var specificationWrapper = $(".product-specification-wrapper");
            if (!specificationWrapper.find("select").length) {
                specificationWrapper.remove();
            }
        },

        categoryWrapper: function () {
            $('.product-name').matchHeight(J.config.sameHeightConfig);
            $('.product-list-description').matchHeight(J.config.sameHeightConfig);
        },

        categoryDescription: function () {
            var categoryDescElement = $(".category-header-wrapper");
            var pagingTopElement = $(".paging-control-box");
            var currentPageNum = getUrlParameters().pagenum;
            if (currentPageNum > 1) {
                categoryDescElement.hide();
            } else {
                categoryDescElement.insertBefore(pagingTopElement);
            }
        },

        orderConfirm: function () {
            if (typeof Checkout == "undefined") {
                // Not a responsive checkout so we need to alter the DOM
                // NOTE: This function will soon be deprecated
                $(".PlacedOrderDetailsFooter").append("<div id='order-details-placeholder'></div>");
                var confirmation = {};
                var str = "";
                var text = $("td.PlacedOrderDetailsShipping").html();
                $("#order-details-placeholder").append(text);
                $("td.PlacedOrderDetailsShipping").remove();
                confirmation.header = $(".PlacedOrderDetailsHeaderCell1").html();
                confirmation.number = $(".PlacedOrderDetailsHeaderCell2:eq(0)").html();
                confirmation.date = $(".PlacedOrderDetailsHeaderCell2:eq(1)").html();
                str += "<div id='order-confirmation-header'>";
                str += confirmation.number;
                str += "<br />";
                str += confirmation.date;
                str += "</div>";
                $(".fastcartconfirmationpagetitle").before(confirmation.header);
                $(".fastcartconfirmationpagetitle").after(str);
                $(".PlacedOrderDetailsHeader table tr:eq(0)").remove();
            }
        },

        stockNotification: function () {
            // Load correct CSS in stock notification popup
            $('.fancybox-iframe-stock-notification').fancybox({
                type: 'iframe',
                autoDimensions: true,
                autoScale: true,
                afterLoad: function () {
                    $("iframe.fancybox-iframe").contents().find("head").append('<link href="' + J.config.cssFallbackUrl + '" rel="stylesheet" type="text/css" />');
                }
            });
        },

        initCart: function () {
            J.cart = {
                cartId: getCookie("JetShop_CartID")
            };
        },

        updateCartData: function (xhr, cartApiData) {
            if (xhr) {
                // ADD DATA ONLY REACHABLE FROM REGULAR CART UPDATE (FREE SHIPPING TEXT)
                var xmlDOM = (new DOMParser()).parseFromString(xhr.responseText, 'text/xml');
                var temp = $(xmlDOM.getElementsByTagName("DivContents")[0].childNodes[1]).text();
                var tempObj = $(temp).text();
                if (tempObj.length > 0) {
                    J.cart.freeFreightMessage = $(temp).text();
                } else {
                    J.cart.freeFreightMessage = false;
                }
            }
            if (cartApiData) {
                // DATA ALREADY EXISTS - UPDATE CART OBJECT
                $.each(cartApiData, function (propName, propValue) {
                    J.cart[propName] = propValue;
                });
                // Trigger event for modules
                $(window).triggerHandler('cart-updated');
            } else {
                // NO EXISTING DATA - GET FROM API
                var restUrl = "/services/rest/v2/json/" + JetshopData.Culture + "/" + JetshopData.Currency + "/shoppingcart/" + J.cart.cartId;
                if (J.checker.isLoggedIn) {
                    restUrl += "?pricelistid=" + JetshopData.PriceListId;
                }
                $.ajax({
                    type: "GET",
                    cache: false,
                    url: restUrl,
                    success: function (data) {
                        var json = typeof data == 'object' ? data : $.parseJSON('(' + data + ')');
                        cartApiData = json[0];
                        // SEND RECEIVED DATA BACK INTO DATA UPDATE FUNCTION
                        J.components.updateCartData(false, cartApiData);
                    },
                    error: function (request, status, error) {
                        console.error("Cart data fetch error:");
                        console.log(error);
                    }
                });
                $(window).triggerHandler('cart-loaded');
            }
        },

        deleteFromCart: function (recId) {
            var restUrl = "/services/rest/v2/json/" + J.data.culture + "/" + J.data.currency.name + "/shoppingcart/delete/" + J.cart.cartId;
            if (JetshopData.IsLoggedIn) {
                restUrl += "?pricelistid=" + J.data.priceListId;
            }
            $.ajax({
                type: "POST",
                cache: false,
                url: restUrl,
                data: '{ "RecId": ' + recId + '}',
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    var json = typeof data == 'object' ? data : $.parseJSON('(' + data + ')');
                    var cartApiData = json[0];
                    J.components.updateCartData(false, cartApiData);
                    Services.cartService.reload();
                    if (J.checker.isCheckoutPage) {
                        location.reload();
                    }
                },
                error: function (request, status, error) {
                    console.error('Ajax Error in J.components.deleteFromCart:');
                    console.log(request);
                    console.log(status);
                    console.log(error);
                }
            });
        },
        emptyCart: function () {
            if (J.cart.ProductsCart.length > 0) {
                for (var item in J.cart.ProductsCart) {
                    J.components.deleteFromCart(J.cart.ProductsCart[item]["RecId"]);
                }
            }
        }

    };
    this.pages = {
        allPages: function () {
            J.components.initCart();
            J.api.helpers.initCache();
            // Product page
            if (J.checker.isProductPage) {
                addBodyClass("product-page-id-" + J.data.productId);
            }
            // Category pages
            if (J.checker.isCategoryPage || J.checker.isCategoryAdvancedPage) {
                addBodyClass("category-page-id-" + J.data.categoryId);
            }
        },
        queues: {
            "all-pages": [],
            "start-page": [],
            "product-page": [],
            "category-advanced-page": [],
            "category-page": [],
            "orderconfirmation-page": [],
            "manufacturer-advanced-page": [],
            "manufacturer-page": [],
            "news-page": [],
            "checkout-page": [],
            "searchresult-page": [],
            "my-page": [],
            "changepassword-page": [],
            "standard-page": [],
            "sitemap-page": []
        },
        addToQueue: function (pageType, queueItem) {
            if (typeof J.pages.queues[pageType] != "undefined") {
                J.pages.queues[pageType].push(queueItem);
            } else {
                console.log("Error: Queue-type not found: " + pageType);
            }
        },
        init: function () {
            J.pages.allPages();
            executeQueue(J.pages.queues["all-pages"]);
            if (typeof J.pages.queues[J.pageType] != "undefined") {
                executeQueue(J.pages.queues[J.pageType]);
            } else {
                console.log("Error: Queue-type not found: " + J.pageType);
            }
        }
    };

    this.api = {
        helpers: {
            baseUrl: function (includeCulture, includeCurrency) {
                var url = window.location.protocol + "//" + window.location.hostname + "/Services/Rest/v2/json/";
                if (includeCulture) url += J.data.culture + "/";
                if (includeCurrency) url += J.data.currency.name + "/";
                return url;
            },
            //TODO when adding cart-services we need to add method postAjax
            getAjax: function (service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency) {
                //console.info("Actually fetching AJAX: " + service);
                $.ajax({
                    type: "GET",
                    url: J.api.helpers.baseUrl(includeCulture, includeCurrency) + service,
                    success: function (data, textStatus, jqXHR) {
                        if (enableCache) {
                            J.api.helpers.save(J.api.helpers.baseUrl(includeCulture, includeCurrency) + service, cacheMinutes, data);
                        }
                        if (typeof callback == "function") {
                            callback(data, callbackOptions);
                        } else if (typeof callback.success == "function") {
                            callback.success(data, callbackOptions, textStatus, jqXHR);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        var error = {"error": "Ajax fail", "service": service};
                        J.api.helpers.raiseError(error);
                        if (typeof callback.error == "function") {
                            callback.error(callbackOptions, xhr, ajaxOptions, thrownError);
                        }
                    },
                    complete: function (event, xhr) {
                        if (typeof callback.complete == "function") {
                            var data = JSON.parse(event.responseText);
                            callback.complete(data, callbackOptions, event, xhr);
                        }
                    }
                });
            },
            getter: function (service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency) {
                if (typeof callback === "undefined") {
                    var error = {"error": "No callback", "service": service};
                    J.api.helpers.raiseError(error);
                } else {
                    if (typeof callback.before == "function") {
                        callback.before(callbackOptions);
                    }
                    if (enableCache) {
                        var itemId = J.api.helpers.baseUrl(includeCulture, includeCurrency) + service;
                        if (J.api.helpers.read(itemId, cacheMinutes)) {
                            var cachedItem = J.api.helpers.read(itemId, cacheMinutes);
                            var minutes = Math.floor(Math.abs(cachedItem.timeStamp - Date.now()) / 60000);
                            if (minutes < cacheMinutes) {
                                var jqXHR = cachedItem.data;
                                var textStatus = "success";
                                if (typeof callback == "function") {
                                    callback(cachedItem.data, callbackOptions);
                                } else if (typeof callback.success == "function") {
                                    callback.success(cachedItem.data, callbackOptions, textStatus, jqXHR);
                                }
                                if (typeof callback.complete == "function") {
                                    callback.complete(cachedItem.data, callbackOptions, textStatus);
                                }
                            } else {
                                J.api.helpers.getAjax(service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency);
                            }
                        } else {
                            J.api.helpers.getAjax(service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency);
                        }
                    } else {
                        J.api.helpers.getAjax(service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency);
                    }
                }
            },
            read: function (id, cacheMinutes) {
                var item = localStorage.getItem(J.config.apiCachePrefix + id);
                if (item === "") {
                    return false;
                } else {
                    return JSON.parse(item);
                }
            },
            save: function (id, cacheMinutes, data) {
                //log("Caching item: " + id);
                var storage = isLocalStorageNameSupported();
                if (storage) {
                    var timeStamp = Date.now();
                    var item = {"cacheMinutes": cacheMinutes, "timeStamp": timeStamp, "data": data};
                    try {
                        localStorage.setItem(J.config.apiCachePrefix + id, JSON.stringify(item));
                    } catch (e) {
                        if (J.api.helpers.isQuotaExceeded(e)) {
                            console.log("Error storing local storage");
                        }
                    }
                }
            },
            clearCache: function () {
                for (var key in window.localStorage) {
                    if (key.indexOf(J.config.apiCachePrefix) != -1) {
                        localStorage.removeItem(key);
                    }
                }
                console.log("Cleared cache.");
            },
            initCache: function () {
                if ($(".vat-selector-input input").length > 0) {
                    var vatInputElementId = $(".vat-selector-input input").attr("id");
                    var vatInputElement = document.getElementById(vatInputElementId);
                    vatInputElement.onclick = function () {
                        J.api.helpers.clearCache();
                        setTimeout('__doPostBack(\'' + vatInputElementId + '\',\'\')', 0);
                    }
                }
            },
            raiseError: function (error) {
                console.error(error);
            },
            isQuotaExceeded: function (error) {
                var quotaExceeded = false;
                if (error) {
                    if (error.code) {
                        switch (error.code) {
                            case 22:
                                quotaExceeded = true;
                                break;
                            case 1014:
                                // Firefox
                                if (error.name === 'NS_ERROR_DOM_QUOTA_REACHED') {
                                    quotaExceeded = true;
                                }
                                break;
                        }
                    } else if (error.number === -2147024882) {
                        // Internet Explorer 8
                        quotaExceeded = true;
                    }
                }
                return quotaExceeded;
            },
            createObject: function (data) {
                return {
                    TotalProducts: data.length,
                    TotalPages: 1,
                    ProductsInPage: data.length,
                    ProductItems: data,
                    PageSize: 0
                };
            }
        },
        product: {
            get: function (callback, callbackOptions, enableCache, cacheMinutes, id) {
                if (typeof id === "number") {
                    var service = "products/" + id;
                    J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes);
                } else {
                    var list = id;
                    var products = [];
                    var add = function (data) {
                        products.push(data.ProductItems[0]);
                        if (list.length == products.length) {
                            var obj = J.api.helpers.createObject(products);
                            callback(obj, callbackOptions);
                        }
                    };
                    for (var i = 0; i < list.length; i++) {
                        J.api.product.get(add, i, enableCache, cacheMinutes, list[i]);
                    }
                }
            },
            getRelatedProducts: function (callback, callbackOptions, enableCache, cacheMinutes, id) {
                var products = [];
                var total = 0;
                var get = function (productData, id) {
                    var list = productData.ProductItems[0].RelatedProducts;
                    total = list.length;
                    for (var i = 0; i < list.length; i++) {
                        J.api.product.get(add, null, enableCache, cacheMinutes, list[i]);
                    }
                };
                var add = function (data) {
                    products.push(data.ProductItems[0]);
                    if (total == products.length) {
                        callback(products, callbackOptions);
                    }
                };
                J.api.product.get(get, id, enableCache, cacheMinutes, id);
            }
        },
        category: {
            get: function (callback, callbackOptions, enableCache, cacheMinutes, id, displayTypeWeb) {
                var service = "categories/" + id;
                if (displayTypeWeb) service += "?displayType=web";
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes);
            },
            getProducts: function (callback, callbackOptions, enableCache, cacheMinutes, id, full, rows, page) {
                var service = "categories/" + id + "/products";
                if (full) service += "/full";
                if (rows) service += "/" + rows;
                if (page) service += "/" + page;
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, true);
            },
            list: function (callback, callbackOptions, enableCache, cacheMinutes) {
                var service = "categories";
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
            }
        },
        page: {
            get: function (callback, callbackOptions, enableCache, cacheMinutes, id) {
                var service = "pages/" + id;
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
            },
            list: function (callback, callbackOptions, enableCache, cacheMinutes) {
                var service = "pages";
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
            }
        },
        search: function (callback, callbackOptions, enableCache, cacheMinutes, string, full, rows, page) {
            var service = "search/";
            if (full) {
                service += "full/" + string;
            } else {
                service += string;
            }
            if (rows) service += "/" + rows;
            if (page) service += "/" + page;
            J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
        },
        settings: {
            get: function (callback, callbackOptions, enableCache, cacheMinutes) {
                var service = "settings";
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, false, false);
            }
        },
        startpage: {
            get: function (callback, callbackOptions, enableCache, cacheMinutes, id) {
                var service = "customstartpage/" + id;
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
            },
            list: function (callback, callbackOptions, enableCache, cacheMinutes) {
                var service = "customstartpages";
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, false, false);
            },
            active: function (callback, callbackOptions, enableCache, cacheMinutes) {
                var service = "activestartpage";
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, false, false);
            }
        }
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Method calls, init and setup
    //
    if (typeof JetshopData !== "undefined") {
        this.data.language = JetshopData.Language;
        this.data.culture = JetshopData.Culture;
        this.data.priceList = JetshopData.PriceList;
        this.data.priceListId = JetshopData.PriceListId;
        this.data.categoryId = JetshopData.CategoryId;
        this.data.productId = JetshopData.ProductId;
        this.checker.isStage = JetshopData.IsStage;
        this.checker.isLoggedIn = JetshopData.IsLoggedIn;
        this.checker.isVatIncluded = JetshopData.VatIncluded;
        this.checker.isCheckoutHttps = JetshopData.IsCheckoutHttps;
        this.data.currency = {
            name: JetshopData.Currency,
            separator: JetshopData.CurrencyDecimalSeparator,
            display: JetshopData.CurrencyDisplay,
            symbol: JetshopData.CurrencySymbol
        };
        this.data.jetshopData = true;
    } else {
        J.data.language = "en";
        J.data.categoryId = 0;
        J.data.productId = 0;
    }
    this.pageType = this.setPageType();
    //
    //  Initialize
    //
    this.init = function () {
        addBodyClass(this.pageType);
        J.components.initFoundation();
        J.components.registerHandlebarHelpers();
        J.components.browserDetect();
        J.switch.check();
        J.pages.init();
        $(document).foundation('reflow');

        // On resize
        $(window).resize(function () {
            J.switch.check();
            J.deviceWidth = J.switch.width;
        });

        // Bind detection of cart update
        $(document).ajaxComplete(function (event, xhr, settings) {
            var data = {};
            if (settings.url === J.config.cartRESTUrl && xhr.readyState == 4 && xhr.status == 200) {
                J.components.updateCartData(xhr, false);
            }
        });

        $(window).trigger('resize');

        // Prevent J.init function from firing twice
        J.init = function () {
            console.error("J.init() triggered twice. Check base.master & script management!");
        };
    };
};

$(document).ready(function () {
    J.init();
});

