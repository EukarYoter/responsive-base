Get related products by Product Id.

Invoked automatically on all pages of type ``product-page``
 
This module will hide the regular "related products" outputted from backend.