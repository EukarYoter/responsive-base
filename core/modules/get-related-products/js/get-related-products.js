var relatedProducts = {
    init: function () {
        if (J.data.productId) {
            J.translations.push(
                {
                    relatedProducts: {
                        sv: "Relaterade produkter",
                        en: "Related products",
                        da: "Relaterede produkter",
                        nb: "Relaterte produkter",
                        fi: "Liittyvät tuotteet"
                    }
                },
                {
                    buyButton: {
                        sv: "Köp",
                        en: "Buy",
                        da: "Købe",
                        nb: "kjøpe",
                        fi: "Ostaa"
                    }
                },
                {
                    infoButton: {
                        sv: "Läs mer",
                        en: "Read more",
                        da: "Läs mer",
                        nb: "Läs mer",
                        fi: "Läs mer"
                    }
                }
            );
            J.api.product.getRelatedProducts(relatedProducts.render, J.data.productId, true, 5, J.data.productId)
        }
    },
    render: function (data, callbackOptions) {
        var template = J.views['get-related-products/get-related-products'];
        var html = template(data);
        $(".product-toolbar").after("<div id='list-category-placeholder'></div>");
        $("#list-category-placeholder").append(html);
        $('.product-name').matchHeight(J.config.sameHeightConfig);
        $('.product-list-description').matchHeight(J.config.sameHeightConfig);
    }
};

J.pages.addToQueue("product-page", relatedProducts.init);

