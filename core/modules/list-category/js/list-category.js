var listCategory = {
    config: {
        categoryId: 158
    },
    init: function () {
        J.translations.push(
            {
                buyButton: {
                    sv: "Köp",
                    en: "Buy",
                    da: "Köp",
                    nb: "Köp",
                    fi: "Köp"
                }
            },
            {
                infoButton: {
                    sv: "Läs mer",
                    en: "Read more",
                    da: "Läs mer",
                    nb: "Läs mer",
                    fi: "Läs mer"
                }
            }
        );
        if (J.data.categoryId === config.categoryId ){
            J.api.category.getProducts(listCategory.renderCategory, config.categoryId, true, 1, config.categoryId,  true, 6, 1);
        }
    },
    renderCategory: function (data, callbackOptions) {
        data.categoryId = callbackOptions;
        var template = J.views['list-category/product-item'];
        var html = template(data);
        $("#list-category-placeholder").append(html);
    }
};

J.pages.addToQueue("product-page", listCategory.init);