Add the following lines to ``core/pages/base.master.htm``

Add code inside block ``#header``  > ``#header-inner``

    <j:shoplogo />
    <div id="menu-wrapper">
        <div id="menu-content">
            <div id="menu-activator" class="menu-icon icon-button"><i class="fa fa-bars fa-fw"></i><span></span></div>
            <div id="search-box-wrapper" class="menu-component">
                <div id="search-activator" class="search-icon icon-button"><i class="fa fa-search fa-fw"></i><span></span></div>
                <j:searchbox />
            </div>
            <div id="cart-activator" class="cart-icon icon-button"><i class="fa fa-shopping-cart fa-fw"></i><span></span><b></b></div>
            <div class="cart-area-wrapper menu-component">
                <j:cart />
                <j:freefreightmessagecart />
                <j:voucher />
            </div>
            <div id="info-text-left-wrapper" class="menu-component hide-for-medium-down">
                <j:infotextleft />
            </div>
        </div>
    </div>
    
    
**Reversion**
    
Remove all elements with class ``.icon-button``     
