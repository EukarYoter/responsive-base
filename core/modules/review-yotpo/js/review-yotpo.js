//
//  Yotpo Reviews
//
var yotpoReview = {
    config: {
        key: "***API_KEY_HERE***"
    },
    name: "Yotpo - Reviews & Star-ratings",
    version: "1.0.0",
    product: {
        init: function () {
            J.translations.push(
                {
                    reviews: {
                        sv: "Recensioner",
                        en: "Reviews"
                    }
                }
            );
            var data = yotpoReview.product.getData();
            yotpoReview.product.renderStars(data);
            yotpoReview.product.renderReview(data);
            yotpoReview.product.injectScript();
        },
        getData: function () {
            return {
                id: J.data.productId,
                name: $(".product-page-header span").text(),
                pageUrl: encodeURIComponent(window.location.href),
                imageUrl: encodeURIComponent($("#FrontImage").attr("src")),
                description: $(".product-short-description-label").text(),
                domain: window.location.host,
                appKey: yotpoReview.config.key
            };
        },
        renderStars: function (data) {
            var template = J.views['review-yotpo/product-stars'];
            var html = template(data);
            $(".product-short-description").before(html);
        },
        renderReview: function (data) {
            var template = J.views['review-yotpo/product-review'];
            var html = template(data);
            $(".product-toolbar").before(html);
        },
        injectScript: function () {
            (function e() {
                var e = document.createElement("script");
                e.type = "text/javascript", e.async = true, e.src = "//staticw2.yotpo.com/" + yotpoReview.config.key + "/widget.js";
                var t = document.getElementsByTagName("script")[0];
                t.parentNode.insertBefore(e, t)
            })();
        }
    },
    order: {
        init: function () {
            var data = yotpoReview.product.getData();
            yotpoReview.order.injectScript(data);
        },
        getData: function () {
            return {
                orderId: dataLayer[0].transactionId,
                orderAmount: dataLayer[0].transactionValue,
                orderCurrency: dataLayer[0].transactionCurrency
            }
        },
        injectScript: function (data) {
            (function e() {
                var e = document.createElement("script");
                e.type = "text/javascript", e.async = true, e.src = "//staticw2.yotpo.com/" + yotpoReview.config.key + "/widget.js";
                var t = document.getElementsByTagName("script")[0];
                t.parentNode.insertBefore(e, t)
            })();
            yotpoTrackConversionData = {
                orderId: data.orderID,
                orderAmount: data.orderAmount,
                orderCurrency: data.orderCurrency
            }
        }
    }
};

J.pages.addToQueue("product-page", yotpoReview.product.init);
J.pages.addToQueue("orderconfirmation-page", yotpoReview.order.init);
