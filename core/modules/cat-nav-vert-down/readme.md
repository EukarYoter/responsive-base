Vertical-down category navigation for large-up widths.

This module is recommended for customers with no level 3 categories.

**Installation**

 Add this j-flow tag to ``pages/base.master.html``

``<j:categorynavigation  AddIndexClasses="true" AddActiveClasses="true" LoadAllCategories="True" />``

**Compatible modules**

- ``cat-nav-mobile``

**Incompatible modules**

- ``cat-nav-vert-mega-dd``
- ``cat-nav-vert-right``
